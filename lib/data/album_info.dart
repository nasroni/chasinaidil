class AlbumInfo {
  final int id;
  final String title;
  final String cover;

  AlbumInfo(this.id, this.title, this.cover);
}
