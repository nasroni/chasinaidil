import 'package:flutter/cupertino.dart';

class Song {
  final int id;
  final String title;
  final String albumId;
  final String fromText;
  final String duration;
  final List<Verse> lyrics;

  int get durationInt {
    return (double.parse(duration)*1000).round();
  }

  Song({String id,
    String title,
    String albumId,
    String psalm,
    String duration,
    List<Verse> text})
      : this.id = int.parse(id),
        this.title = title,
        this.albumId = albumId,
        this.fromText = psalm,
        this.duration = duration,
        this.lyrics = text;

  factory Song.fromJson(Map<String, dynamic> parsedJson) {
    var verses = parsedJson["text"] as List;
    List<Verse> verseList = verses.map((i) => Verse.fromJson(i)).toList();

    return Song(
        id: parsedJson['id'],
        title: parsedJson['title'],
        albumId: parsedJson['album'].toString(),
        psalm: parsedJson['psalm'],
        duration: parsedJson['duration'].toString(),
        text: verseList,
    );
  }
}

class Verse {
  final int number;
  final bool chorus;
  final bool chotima;
  final String content;

  Verse({int no, String type, String content})
      : this.number = no,
        this.chorus = type == "chorus" ? true : false,
        this.chotima = type == "chotima" ? true : false,
        this.content = content;

  factory Verse.fromJson(Map<String, dynamic> parsedJson) {
    return Verse(
        no: parsedJson["no"],
        type: parsedJson["type"],
        content: parsedJson["content"]
    );
  }
}
