class Lists {
  static final album = [
    // {"title": 'Ҳама', "cover": "00.jpg", "id": 0},
    // {"title": 'Дӯстдошта', "cover": "fa.jpg", "id": 17},
    {"title": 'Исо омадааст', "cover": "01.jpg", "id": 1},
    {"title": 'Чашмаи ҳаёт', "cover": "02.jpg", "id": 2},
    {"title": 'Дар шаби охир', "cover": "03.jpg", "id": 3},
    {"title": 'Нури ҷаҳон', "cover": "04.jpg", "id": 4},
    {"title": 'Барраи Худо', "cover": "05.jpg", "id": 5},
    {"title": 'Моҳигир', "cover": "06.jpg", "id": 6},
    {"title": 'Шарирон', "cover": "07.jpg", "id": 7},
    {"title": 'Худои воҷибулвуҷуд', "cover": "08.jpg", "id": 8},
    {"title": 'Пари... меҳрубон', "cover": "09.jpg", "id": 9},
    {"title": 'Эй Наҷоткор', "cover": "10.jpg", "id": 10},
    {"title": 'Ту кӯзагарӣ', "cover": "11.jpg", "id": 11},
    {"title": 'Бимон бо ман', "cover": "12.jpg", "id": 12},
    {"title": 'Дурӯзаи умрам', "cover": "13.jpg", "id": 13},
    {"title": 'Чӯпони некӯ', "cover": "14.jpg", "id": 14},
    {"title": 'Худоё, бубахшо', "cover": "15.jpg", "id": 15},
    {"title": 'Пирӯзӣ бар марг', "cover": "16.jpg", "id": 16},
  ];
  static get albumWF {
    var list = album.map((e) => e).toList();
    list.insert(0, {"title": 'Дӯстдошта', "cover": "fa.jpg", "id": 17});
    return list;
  }
}
