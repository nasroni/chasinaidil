import 'package:audio_service/audio_service.dart';
import 'package:chasinaidil/helpers/tj.dart';
import 'package:chasinaidil/pages/album_page.dart';
import 'package:chasinaidil/pages/download_page.dart';
import 'package:chasinaidil/pages/settings_page.dart';
import 'package:chasinaidil/pages/song_page.dart';
import 'package:chasinaidil/pages/start_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:just_audio/just_audio.dart';

void main() => runApp(App());

Color colorGolden = Colors.amber[300];
Color colorBlue = Color.fromRGBO(0, 46, 171, 1.0);

class App extends StatelessWidget {
  static final player = AudioPlayer();
  static List<String> playList = [];
  static int currentInPlayList = 0;

  @override
  Widget build(BuildContext context) {
    return AudioServiceWidget(
      child: MaterialApp(
        title: 'Хазинаи дил',
        locale: Locale('tj'),
        supportedLocales: [Locale('tj')],
        localizationsDelegates: [
          TjLocalizationsDelegate(),
          GlobalWidgetsLocalizations.delegate,
        ],
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: colorBlue,
          accentColor: colorBlue,
          primaryTextTheme: TextTheme(
            title: TextStyle(
                color: colorGolden, fontSize: 35, fontFamily: 'Pattaya'),
            body1: TextStyle(
              color: colorGolden,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.4,
            ),
          ),
        ),
        darkTheme: ThemeData(
          backgroundColor: Colors.grey[900],
          canvasColor: Colors.grey[900],
          primaryColor: colorBlue,
          accentColor: colorBlue,
          textTheme:
              Theme.of(context).textTheme.apply(bodyColor: Colors.white70),
          primaryTextTheme: TextTheme(
            //headline6: TextStyle(
            title: TextStyle(
                color: colorGolden, fontSize: 35, fontFamily: 'Pattaya'),
            body1: TextStyle(
              color: colorGolden,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.4,
            ),
            body2: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
        //home: StartPage(),
        initialRoute: '/',
        /*routes: {
          '/': (context) => StartPage(),
          AlbumPage.routeName: (context) => AlbumPage(),
          SongPage.routeName: (context) => SongPage(),
        },*/
        /*onGenerateRoute: (settings) {
          switch (settings.name) {
            case '/':
              return MaterialPageRoute(
                builder: (_) => StartPage(),
              );
              break;
            case '/album':
              return PageTransition(
                child: AlbumPage(),
                type: PageTransitionType.rightToLeft,
                settings: settings,
              );
              break;
            case '/song':
              return PageTransition(
                child: SongPage(),
                type: PageTransitionType.downToUp,
                settings: settings,
              );
              break;
            default:
              return null;
          }
        },*/
        onGenerateRoute: (RouteSettings settings) {
          return new PageRouteBuilder<dynamic>(
            settings: settings,
            pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondaryAnimation) {
              switch (settings.name) {
                case '/':
                  return StartPage();
                  break;
                case '/album':
                  return AlbumPage();
                  break;
                case '/song':
                  return SongPage();
                  break;
                case '/settings':
                  return SettingsPage();
                  break;
                case '/download':
                  return DownloadPage();
                  break;
                default:
                  return null;
              }
            },
            transitionDuration: const Duration(milliseconds: 250),
            transitionsBuilder: (BuildContext context,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
                Widget child) {
              switch (settings.name) {
                case '/song':
                  return effectMap[PageTransitionType.slideInUp](
                      Curves.linear, animation, secondaryAnimation, child);
                  break;
                default:
                  return effectMap[PageTransitionType.slideInLeft](
                      Curves.linear, animation, secondaryAnimation, child);
              }
            },
          );
        },
      ),
    );
  }
}
