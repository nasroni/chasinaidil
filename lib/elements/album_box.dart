import 'dart:io';
import 'package:chasinaidil/data/album_info.dart';
import 'package:chasinaidil/services/preferences.dart';
import 'package:chasinaidil/services/songs.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:toast/toast.dart';

class AlbumBox extends StatefulWidget {
  final int id;
  final String title;
  final String cover;
  final bool download;
  final bool open;

  AlbumBox({id, title, cover, action})
      : this.id = id,
        this.title = title,
        this.cover = cover,
        this.download = action == "download",
        this.open = action == "open";

  AlbumBoxState createState() => AlbumBoxState();
}

class AlbumBoxState extends State<AlbumBox> {
  /* ~ Stuff for downloading ~ */
  bool _allowWriteFile = false;
  int downloadProgress = 0;

  static const String SONG_PREFIX = 'Хазинаи-дил-';
  static const String FOLDER_PREFIX = 'Хазинаи дил ';
  static const String FOLDER_FAVORITES = 'Дӯстдошта';
  static const String SONG_SERVER = 'chasinaidil.nasroni.one/mp3/all';
  static const String EXT_DIR = '/storage/emulated/0';
  static const String FOLDER = 'Хазинаи дил';

  Future get _localFolder async {
    var localFolder = Directory('$EXT_DIR/$FOLDER');
    if (await localFolder.exists()) return localFolder.path;
    final Directory newLocalFolder = await localFolder.create(recursive: false);
    return newLocalFolder.path;
  }

  Future _albumFolder(String id) async {
    var localPath = await _localFolder;
    var tempFolder = Directory('$localPath/$FOLDER_PREFIX$id');
    if (id == "17") tempFolder = Directory('$localPath/$FOLDER_FAVORITES');
    if (await tempFolder.exists()) return tempFolder.path;
    final Directory newAlbumFolder = await tempFolder.create(recursive: true);
    return newAlbumFolder.path;
  }

  Future downloadAlbum() async {
    HttpClient client = new HttpClient();
    setState(() {
      downloadProgress = 1;
    });
    // Get album song list
    List<dynamic> songs;
    if (widget.id == 17)
      songs = await Preferences.likedSongs;
    else
      songs = await Songs.listAlbumIds(widget.id);

    var count = 0;
    var countAll = songs.length;

    for (String songId in songs) {
      await _downloadFile(songId, client);
      setState(() {
        count++;
        downloadProgress = ((count / countAll) * 100).round();
      });
    }

    client.close();
    setState(() {
      downloadProgress = 100;
    });
  }

  Future _downloadFile(String songNr, HttpClient client) async {
    while (songNr.length != 3) songNr = "0" + songNr;

    // Check if file doesn't already exists. If it does -> cancel with return null
    var fileExists = await _checkIfFile(songNr);
    if (fileExists == null || fileExists) return null;

    // Preparation
    var localFolder = await _albumFolder(widget.id.toString());

    // Download file
    var remoteFileString = 'https://$SONG_SERVER/$SONG_PREFIX$songNr.mp3';
    var localFileString = '$localFolder/$SONG_PREFIX$songNr.mp3';

    await client
        .getUrl(Uri.parse(remoteFileString))
        .then((HttpClientRequest request) => request.close())
        .then((HttpClientResponse response) =>
            response.pipe(new File(localFileString).openWrite()));
  }

  Future _checkIfFile(String songNr) async {
    if (!_allowWriteFile) return false;
    final localFolder = await _albumFolder(widget.id.toString());
    var localFileString = '$localFolder/$SONG_PREFIX$songNr.mp3';
    final file = File(localFileString);
    final bool exists = file.existsSync();
    return exists;
  }

  /* ~ End stuff ~ */

  void initState() {
    super.initState();
  }

  static Radius _circularR5 = Radius.circular(5.0);
  final BorderRadius _bottomRoundBorder5 =
      BorderRadius.only(bottomRight: _circularR5, bottomLeft: _circularR5);

  @override
  Widget build(BuildContext context) {
    bool darkMode =
        MediaQuery.of(context).platformBrightness == Brightness.dark;
    bool formatPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    return GestureDetector(
      onTap: () async {
        if (widget.open) {
          Navigator.pushNamed(context, '/album',
              arguments: AlbumInfo(widget.id, widget.title, widget.cover));
        }
        if (widget.download) {
          if (widget.id == 0) {
            Toast.show('Not yet implemented', context);
            return null;
          }
          await requestWritePermission();
          downloadAlbum();
        }
      },
      child: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          Container(
            width: 145,
            alignment: Alignment(0, 0),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: _bottomRoundBorder5,
              boxShadow: [
                BoxShadow(
                    color: darkMode ? Colors.grey[800] : Colors.grey[500],
                    offset: Offset(2.0, 2.0))
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: _bottomRoundBorder5,
                  child: Image.asset(
                    'assets/covers/cd_' + widget.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Text(
                    widget.title.toString(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).primaryTextTheme.body1,
                  ),
                ),
              ],
            ),
          ),
          widget.download
              ? Opacity(
                  opacity: 0.5,
                  child: Container(
                    color:
                        downloadProgress == 100 ? Colors.green : Colors.black,
                    width: 145,
                    height: formatPortrait ? 180 : 193,
                  ),
                )
              : Container(),
          widget.download
              ? Container(
                  width: 145,
                  height: formatPortrait ? 180 : 193,
                  child: Center(
                    child: downloadProgress != 0
                        ? Text(
                            downloadProgress == 100
                                ? '✓'
                                : 'loading ... $downloadProgress%',
                            style: TextStyle(color: Colors.white),
                          )
                        : Icon(
                            Icons.file_download,
                            color: Colors.white,
                          ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  /* Stuff for file download */

  requestWritePermission() async {
    var handler = PermissionHandler();
    var permission =
        await handler.checkPermissionStatus(PermissionGroup.storage);
    if (permission != PermissionStatus.granted) {
      // Requesting Rights
      var permissions =
          await handler.requestPermissions([PermissionGroup.storage]);
      if (permissions[PermissionGroup.storage] != PermissionStatus.granted) {
        // Rights denied
      } else {
        // Rights granted
        setState(() {
          _allowWriteFile = true;
        });
      }
    } else {
      // Rights already granted
      setState(() {
        _allowWriteFile = true;
      });
    }
  }
}
