import 'package:chasinaidil/data/song.dart';
import 'package:flutter/material.dart';

class SongTextView extends StatelessWidget {
  final List<Verse> lyrics;
  final double textSize;
  final bool darkMode;
  final bool chords;

  SongTextView({this.lyrics, this.textSize, this.darkMode, this.chords});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemBuilder: (context, position) {
          var verse = lyrics[position];
          bool lastOne;
          lyrics.length == position + 1 ? lastOne = true : lastOne = false;
          return Container(
            child: Padding(
              padding: lastOne
                  ? EdgeInsets.symmetric(vertical: 8.0)
                  : EdgeInsets.only(top: 8.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      verse.chorus || verse.chotima
                          ? Expanded(
                              child: Text(
                                verse.chotima ? 'Хотима' : 'Бандгардон',
                                style: TextStyle(
                                  fontFamily: 'Pacifico',
                                  color: Colors.grey,
                                  fontStyle: FontStyle.italic,
                                  fontSize: textSize,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            )
                          : Expanded(
                              child: Text(
                                "Банд " + verse.number.toString(),
                                style: TextStyle(
                                  fontFamily: 'Pacifico',
                                  color: Colors.grey,
                                  fontStyle: FontStyle.italic,
                                  fontSize: textSize,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Center(
                          child: Padding(
                            child: Text(
                              lyrics[position].content,
                              textAlign: TextAlign.center,
                              maxLines: 30,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: darkMode
                                    ? Colors.grey[50]
                                    : Colors.grey[800],
                                fontSize: textSize,
                                fontFamily: chords ? 'SourceCodePro' : null,
                              ),
                            ),
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
        itemCount: lyrics.length,
      ),
    );
  }
}
