import 'package:audio_service/audio_service.dart';
import 'package:chasinaidil/data/song.dart';
import 'package:chasinaidil/elements/audio_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SongBox extends StatefulWidget {
  final String title;
  final int number;
  final String duration;
  final Song song;
  final bool liked;

  SongBox({Song song, bool liked})
      : this.title = song.title,
        this.number = song.id,
        this.duration = song.duration,
        this.song = song,
        this.liked = liked;

  _SongBoxState createState() => _SongBoxState();
}

String secondsToText(String input) {
  double number = double.tryParse(input);
  if (number == null) {
    return "--:--";
  }
  int integer = number.round();
  int minutes = (integer / 60).round();
  int seconds = (integer % 60);
  String secondsString = seconds.toString();
  if (secondsString.length == 1) {
    secondsString = "0" + secondsString;
  } else if (secondsString.length == 0) {
    secondsString = "00";
  }
  String time = minutes.toString() + ":" + secondsString;
  return time;
}

class _SongBoxState extends State<SongBox> {
  @override
  Widget build(BuildContext context) {
    String duration = secondsToText(widget.duration);
    bool darkMode = MediaQuery.of(context).platformBrightness == Brightness.dark
        ? true
        : false;
    return InkWell(
      splashColor: Colors.amber,
      onTap: () =>
          Navigator.pushNamed(context, '/song', arguments: widget.song),
      onLongPress: () {
        PlayActionButton.play(context, int.parse(widget.song.albumId), widget.song.id);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: darkMode ? Colors.white70 : Colors.black12,
              width: darkMode ? 0.2 : 0.5,
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 13.0),
          child: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: 25,
                child: Text(
                  widget.number.toString(),
                  textAlign: TextAlign.right,
                ),
              ),
              Expanded(
                //width: MediaQuery.of(context).size.width * 0.7,
                //alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Row(
                    children: <Widget>[
                      widget.liked
                          ? Icon(
                              Icons.star,
                              size: 15,
                              color: Colors.amber[300],
                            )
                          : Container(),
                      Expanded(
                        child: Padding(
                          padding: widget.liked
                              ? EdgeInsets.only(left: 2.0)
                              : EdgeInsets.all(0),
                          child: Text(
                            widget.title,
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                width: 50,
                alignment: Alignment.centerRight,
                child: Text(
                  duration,
                  textAlign: TextAlign.right,
                ),
              ),
            ],
          )),
        ),
      ),
    );
  }
}
