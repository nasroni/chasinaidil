import 'package:flutter/material.dart';
import 'package:swipedetector/swipedetector.dart';

class SwipeAppBar extends StatelessWidget implements PreferredSizeWidget {
  final VoidCallback onSwipeDown;
  final VoidCallback onSwipeRight;
  final AppBar appBar;

  const SwipeAppBar({Key key, this.onSwipeDown, this.onSwipeRight, this.appBar}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return  SwipeDetector(onSwipeDown: onSwipeDown, onSwipeRight: onSwipeRight,child: appBar);
  }

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}