import 'package:flutter/material.dart';

class DownloadItem extends StatelessWidget {
  final int id;
  final String albumName;
  final int downloadPercentage;
  final String image;
  final bool downloading;
  final Function(int) clickAction;

  DownloadItem(this.id,
      {@required this.albumName,
      @required this.downloadPercentage,
      @required this.image,
      @required this.downloading,
      @required this.clickAction});

  @override
  Widget build(BuildContext context) {
    bool darkMode = MediaQuery.of(context).platformBrightness == Brightness.dark
        ? true
        : false;

    String albumNameText;
    if (id != 0)
      albumNameText = "${id.toString()} $albumName";
    else
      albumNameText = albumName;
    return InkWell(
      splashColor: Colors.amber,
      onTap: downloadPercentage == 100 ? null : () => clickAction(id),
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
                color: darkMode ? Colors.white70 : Colors.black12,
                width: darkMode ? 0.2 : 0.5),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset(
              'assets/covers/cd_' + image,
              height: 75,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      child: Text(
                        albumNameText,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 22),
                      ),
                    ),
                    Container(
                      child: Text('$downloadPercentage% боргирифта'),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: downloadPercentage == 100
                    ? Icon(
                        Icons.check,
                        color: Colors.green,
                      )
                    : downloading
                        ? Container(
                            height: 20,
                            width: 20,
                            child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(Colors.black),
                              strokeWidth: 2.0,
                            ),
                          )
                        : Icon(Icons.file_download),
              ),
            )
          ],
        ),
      ),
    );
  }
}
