import 'dart:async';
import 'dart:math';

import 'package:audio_service/audio_service.dart';
import 'package:chasinaidil/services/audiofiles.dart';
import 'package:chasinaidil/services/songs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:just_audio/just_audio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:toast/toast.dart';

MediaControl playControl = MediaControl(
  androidIcon: 'drawable/ic_play_arrow',
  label: 'Play',
  action: MediaAction.play,
);
MediaControl pauseControl = MediaControl(
  androidIcon: 'drawable/ic_pause',
  label: 'Pause',
  action: MediaAction.pause,
);
MediaControl skipToNextControl = MediaControl(
  androidIcon: 'drawable/ic_skip_next',
  label: 'Next',
  action: MediaAction.skipToNext,
);
MediaControl skipToPreviousControl = MediaControl(
  androidIcon: 'drawable/ic_skip_previous',
  label: 'Previous',
  action: MediaAction.skipToPrevious,
);
MediaControl stopControl = MediaControl(
  androidIcon: 'drawable/ic_stop',
  label: 'Stop',
  action: MediaAction.stop,
);

Stream<ScreenState> get _screenStateStream =>
    Rx.combineLatest3<List<MediaItem>, MediaItem, PlaybackState, ScreenState>(
        AudioService.queueStream,
        AudioService.currentMediaItemStream,
        AudioService.playbackStateStream,
        (queue, mediaItem, playbackState) =>
            ScreenState(queue, mediaItem, playbackState));

class PlayActionButton extends StatelessWidget {
  final album;
  var song;

  PlayActionButton(this.album);

  PlayActionButton.song(this.song, this.album);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ScreenState>(
        stream: _screenStateStream,
        builder: (context, snapshot) {
          final screenState = snapshot.data;
          final state = screenState?.playbackState;
          final mediaItem = screenState?.mediaItem;
          final basicState = state?.basicState ?? BasicPlaybackState.none;

          if (basicState == BasicPlaybackState.none ||
              (song == null && album.toString() != mediaItem.genre) ||
              (song != null &&
                  song.toString() != mediaItem.title.split(' ')[0])) {
            return FloatingActionButton(
              tooltip: 'Кушодан',
              onPressed: () => play(context, album, song),
              backgroundColor: Colors.amber,
              child: Icon(
                Icons.play_arrow,
                size: 28.0,
                color: Color.fromRGBO(0, 46, 171, 1.0),
              ),
            );
          } else {
            return Container();
          }
        });
  }

  static play(context, album, song) async {
    if (!Audiofiles.allowWriteFile) await Audiofiles.requestWritePermission();
    if (await Audiofiles.checkDownloadAlbum(album) < 100) {
      Toast.show('Шумо бояд аввал дискро боргиред', context,
          duration: Toast.LENGTH_LONG);
      Navigator.of(context).pushNamed('/download');
    } else {
      //if (AudioService.playbackState == null) {
      // AUDIO
      await AudioService.start(
        backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint,
        androidNotificationChannelName: 'Хазинаи дил',
        notificationColor: 0xFF2196f3,
        androidNotificationIcon: 'mipmap/ic_launcher',
        enableQueue: true,
      );
      if (song == null) {
        var queue = await Audiofiles.getQueue(album);
        await AudioService.replaceQueue(queue);
        await AudioService.play();
      } else {
        if (album.toString() != AudioService.currentMediaItem?.genre)
          await AudioService.replaceQueue(await Audiofiles.getQueue(album));
        var mdit = await Audiofiles.getSong(song, album);
        await AudioService.playFromMediaId(mdit[0].id);
      }
    }
  }
}

class ScreenState {
  final List<MediaItem> queue;
  final MediaItem mediaItem;
  final PlaybackState playbackState;

  ScreenState(this.queue, this.mediaItem, this.playbackState);
}

class AudioBar extends StatefulWidget {
  final albumId;

  AudioBar({
    this.albumId,
  });

  @override
  _AudioBarState createState() => _AudioBarState();
}

class _AudioBarState extends State<AudioBar> {
  // Audio
  final BehaviorSubject<double> _dragPositionSubject =
      BehaviorSubject.seeded(null);

  // UI
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: GestureDetector(
        onTap: () => openAudioPlayer(),
        onVerticalDragEnd: (e) {
          if (e.primaryVelocity < -100) openAudioPlayer();
        },
        child: StreamBuilder<ScreenState>(
            stream: _screenStateStream,
            builder: (context, snapshot) {
              final screenState = snapshot.data;
              final queue = screenState?.queue;
              final mediaItem = screenState?.mediaItem;
              final state = screenState?.playbackState;
              final basicState = state?.basicState ?? BasicPlaybackState.none;

              final playing = basicState == BasicPlaybackState.playing;
              final bool hasNext =
                  state?.actions?.contains(MediaAction.skipToNext) ?? false;
              final bool showPrevious =
                  state?.actions?.contains(MediaAction.skipToPrevious) ??
                      false && !hasNext;

              var albumPic = mediaItem?.genre?.toString() ?? "";
              if (albumPic != "") {
                while (albumPic.length != 2) albumPic = "0" + albumPic;
              }

              if (basicState != BasicPlaybackState.none) {
                return Container(
                  height: 65,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 20,
                          color: Colors.black38,
                          offset: Offset(0, 5))
                    ],
                  ),
                  child: isExpanded
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Center(
                              child: Icon(Icons.close),
                            ),
                            Center(
                              child: Text('Пӯшидан'),
                            )
                          ],
                        )
                      : Column(
                          children: [
                            Container(
                              height: 2,
                              alignment: Alignment.centerLeft,
                              child: StreamBuilder(
                                  stream:
                                      Rx.combineLatest2<double, double, double>(
                                          _dragPositionSubject.stream,
                                          Stream.periodic(
                                              Duration(milliseconds: 200)),
                                          (dragPosition, _) => dragPosition),
                                  builder: (context, snapshot) {
                                    double position = (snapshot.data ??
                                            state?.currentPosition
                                                ?.toDouble() ??
                                            0) /
                                        1000;
                                    double duration =
                                        (mediaItem?.duration ?? 1).toDouble() /
                                            1000;
                                    int percentage =
                                        ((position / duration) * 100).round();

                                    return Container(
                                      height: 2,
                                      color: Colors.blue[800],
                                      width:
                                          (MediaQuery.of(context).size.width *
                                                  percentage) /
                                              100,
                                    );
                                  }),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Image.asset(
                                  'assets/covers/cd_' + albumPic + ".jpg",
                                  height: 62,
                                ),
                                Expanded(
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    child: Text(
                                      mediaItem.title,
                                      style: TextStyle(fontSize: 18),
                                      maxLines: 2,
                                    ),
                                  ),
                                ),
                                IconButton(
                                  icon: Icon(
                                    playing ? Icons.pause : Icons.play_arrow,
                                    color: Colors.black,
                                  ),
                                  onPressed: () => playing
                                      ? AudioService.pause()
                                      : AudioService.play(),
                                ),
                                if (hasNext)
                                  IconButton(
                                    icon: Icon(
                                      Icons.skip_next,
                                      color: Colors.black,
                                    ),
                                    onPressed: () => AudioService.skipToNext(),
                                  ),
                              ],
                            ),
                          ],
                        ),
                );
              } else {
                return Container(
                  height: 0,
                );
              }
            }),
      ),
    );
  }

  openAudioPlayer() async {
    if (isExpanded) {
      Navigator.of(context).pop();
    } else {
      var ctrlSheet = showBottomSheet(
        context: context,
        builder: (context) => AudioPlayerView(
          toggleFun: openAudioPlayer,
        ),
      );
      ctrlSheet.closed.then((value) => setState(() {
            isExpanded = false;
          }));
      setState(() {
        isExpanded = true;
      });
    }
  }
}

class AudioPlayerView extends StatefulWidget {
  final Function toggleFun;

  AudioPlayerView({Key key, @required this.toggleFun}) : super(key: key);

  @override
  _AudioPlayerViewState createState() => _AudioPlayerViewState();
}

class _AudioPlayerViewState extends State<AudioPlayerView> {
  final BehaviorSubject<double> _dragPositionSubject =
      BehaviorSubject.seeded(null);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ScreenState>(
        stream: _screenStateStream,
        builder: (context, snapshot) {
          final screenState = snapshot.data;
          final queue = screenState?.queue;
          final mediaItem = screenState?.mediaItem;
          final state = screenState?.playbackState;
          final basicState = state?.basicState ?? BasicPlaybackState.none;

          final playing = basicState == BasicPlaybackState.playing;
          final bool hasNext =
              state?.actions?.contains(MediaAction.skipToNext) ?? false;
          final bool hasPrevious =
              state?.actions?.contains(MediaAction.skipToPrevious) ?? false;

          var albumPic = mediaItem?.genre?.toString() ?? "";
          if (albumPic != "") {
            while (albumPic.length != 2) albumPic = "0" + albumPic;
          }

          return Container(
            height: 210,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[300],
                  blurRadius: 3.0,
                  spreadRadius: 2.0,
                  offset: Offset(0, -2.5),
                ),
              ],
            ),
            child: Material(
              color: Colors.transparent,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Container(
                      height: 8,
                      width: 50,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(4.0),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: Container(
                              width: 260,
                              child: RaisedButton.icon(
                                icon: Icon(FontAwesomeIcons.alignCenter),
                                label: Expanded(
                                    child: Text(
                                  mediaItem?.title ?? "",
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                )),
                                color: Colors.white,
                                onPressed: () async {
                                  var song = await Songs.song(int.parse(
                                      mediaItem?.title?.split(' ')[0] ?? 0));
                                  widget.toggleFun();
                                  Navigator.pushNamed(context, '/song',
                                      arguments: song);
                                },
                              ),
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.skip_previous),
                              iconSize: 35,
                              onPressed: hasPrevious
                                  ? () => AudioService.skipToPrevious()
                                  : null,
                            ),
                            IconButton(
                              icon: Icon(
                                  playing ? Icons.pause : Icons.play_arrow),
                              iconSize: 55,
                              onPressed: () => playing
                                  ? AudioService.pause()
                                  : AudioService.play(),
                            ),
                            IconButton(
                              icon: Icon(Icons.skip_next),
                              iconSize: 35,
                              onPressed: hasNext
                                  ? () => AudioService.skipToNext()
                                  : null,
                            ),
                          ],
                        ),
                        Container(
                          child: positionIndicator(mediaItem, state),
                          height: 65,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  String formattedTime(double time) {
    var minutes = (time / 60).toString().split('.')[0];
    if (minutes.length != 1) minutes = minutes[0];
    var seconds = (time % 60).toStringAsFixed(0);
    if (seconds.length != 2) seconds = "0" + seconds;
    return "$minutes:$seconds";
  }

  Widget positionIndicator(MediaItem mediaItem, PlaybackState state) {
    double seekPos;
    return StreamBuilder(
      stream: Rx.combineLatest2<double, double, double>(
          _dragPositionSubject.stream,
          Stream.periodic(Duration(milliseconds: 200)),
          (dragPosition, _) => dragPosition),
      builder: (context, snapshot) {
        double position =
            (snapshot.data ?? state?.currentPosition?.toDouble() ?? 0);
        double duration = (mediaItem?.duration ?? 0).toDouble();

        String durationS = formattedTime(duration / 1000);
        String positionS =
            formattedTime(seekPos ?? max(0.0, min(position, duration)) / 1000);
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[Text("${positionS}"), Text("${durationS}")],
              ),
            ),
            if (duration != null)
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                    inactiveTrackColor: Colors.amber[50],
                    activeTrackColor: Colors.amber[500],
                    trackHeight: 10,
                    thumbColor: Color.fromRGBO(0, 46, 171, 1.0),
                    thumbShape: RoundSliderThumbShape(enabledThumbRadius: 10)),
                child: Slider(
                  min: 0.0,
                  max: duration,
                  value: seekPos ?? max(0.0, min(position, duration)),
                  onChanged: (value) {
                    _dragPositionSubject.add(value);
                  },
                  onChangeEnd: (value) {
                    AudioService.seekTo(value.toInt());
                    // Due to a delay in platform channel communication, there is
                    // a brief moment after releasing the Slider thumb before the
                    // new position is broadcast from the platform side. This
                    // hack is to hold onto seekPos until the next state update
                    // comes through.
                    // TODO: Improve this code.
                    seekPos = value;
                    _dragPositionSubject.add(null);
                  },
                ),
              ),
          ],
        );
      },
    );
  }
}

void _audioPlayerTaskEntrypoint() {
  AudioServiceBackground.run(() => AudioPlayerTask());
}

class AudioPlayerTask extends BackgroundAudioTask {
  //AudioPlayerTask(this._queue);
  List<MediaItem> _queue;
  int _queueIndex = -1;
  AudioPlayer _audioPlayer = new AudioPlayer();
  Completer _completer = Completer();
  BasicPlaybackState _skipState;
  bool _playing;

  bool get hasNext => _queueIndex + 1 < _queue.length;

  bool get hasPrevious => _queueIndex > 0;

  MediaItem get mediaItem => _queue[_queueIndex];

  BasicPlaybackState _stateToBasicState(AudioPlaybackState state) {
    switch (state) {
      case AudioPlaybackState.none:
        return BasicPlaybackState.none;
      case AudioPlaybackState.stopped:
        return BasicPlaybackState.stopped;
      case AudioPlaybackState.paused:
        return BasicPlaybackState.paused;
      case AudioPlaybackState.playing:
        return BasicPlaybackState.playing;
      case AudioPlaybackState.connecting:
        return _skipState ?? BasicPlaybackState.connecting;
      case AudioPlaybackState.completed:
        return BasicPlaybackState.stopped;
      default:
        throw Exception("Illegal state");
    }
  }

  @override
  Future<void> onStart() async {
    var playerStateSubscription = _audioPlayer.playbackStateStream
        .where((state) => state == AudioPlaybackState.completed)
        .listen((state) {
      _handlePlaybackCompleted();
    });
    var eventSubscription = _audioPlayer.playbackEventStream.listen((event) {
      final state = _stateToBasicState(event.state);
      if (state != BasicPlaybackState.stopped) {
        _setState(
          state: state,
          position: event.position.inMilliseconds,
        );
      }
    });

    /*AudioServiceBackground.setQueue(_queue);
    await onSkipToNext();*/
    await _completer.future;
    playerStateSubscription.cancel();
    eventSubscription.cancel();
  }

  void _handlePlaybackCompleted() {
    if (hasNext) {
      onSkipToNext();
    } else {
      onStop();
    }
  }

  void playPause() {
    if (AudioServiceBackground.state.basicState == BasicPlaybackState.playing)
      onPause();
    else
      onPlay();
  }

  @override
  Future<void> onSkipToNext() => _skip(1);

  @override
  Future<void> onSkipToPrevious() => _skip(-1);

  Future<void> _skip(int offset) async {
    final newPos = _queueIndex + offset;
    if (!(newPos >= 0 && newPos < _queue.length)) return;
    if (_playing == null) {
      // First time, we want to start playing
      _playing = true;
    } else if (_playing) {
      // Stop current item
      await _audioPlayer.stop();
    }
    // Load next item
    _queueIndex = newPos;
    AudioServiceBackground.setMediaItem(mediaItem);
    _skipState = offset > 0
        ? BasicPlaybackState.skippingToNext
        : BasicPlaybackState.skippingToPrevious;
    await _audioPlayer.setUrl(mediaItem.id);
    _skipState = null;
    // Resume playback if we were playing
    if (_playing) {
      onPlay();
    } else {
      _setState(state: BasicPlaybackState.paused);
    }
  }

  @override
  void onPlay() {
    if (_skipState == null) {
      _playing = true;
      _audioPlayer.play();
      _setState(state: BasicPlaybackState.playing);
    }
  }

  @override
  void onPause() {
    if (_skipState == null) {
      _playing = false;
      _audioPlayer.pause();
      _setState(state: BasicPlaybackState.paused);
    }
  }

  @override
  void onSeekTo(int position) {
    _audioPlayer.seek(Duration(milliseconds: position));
  }

  @override
  void onClick(MediaButton button) {
    playPause();
  }

  @override
  void onStop() {
    _audioPlayer.stop();
    _setState(state: BasicPlaybackState.stopped);
    _completer.complete();
  }

  void _setState({@required BasicPlaybackState state, int position}) {
    if (position == null) {
      position = _audioPlayer.playbackEvent.position.inMilliseconds;
    }
    AudioServiceBackground.setState(
      controls: getControls(state),
      systemActions: [MediaAction.seekTo],
      basicState: state,
      position: position,
    );
  }

  List<MediaControl> getControls(BasicPlaybackState state) {
    if ((_playing ?? false) && hasPrevious && hasNext)
      return [
        skipToPreviousControl,
        pauseControl,
        skipToNextControl,
        stopControl,
      ];
    else if ((_playing ?? false) && hasPrevious)
      return [
        stopControl,
        skipToPreviousControl,
        pauseControl,
      ];
    else if ((_playing ?? false) && hasNext)
      return [
        pauseControl,
        stopControl,
        skipToNextControl,
      ];
    else if ((_playing ?? false))
      return [
        stopControl,
        pauseControl,
      ];
    else if (hasPrevious && hasNext)
      return [
        skipToPreviousControl,
        playControl,
        skipToNextControl,
        stopControl,
      ];
    else if (hasPrevious)
      return [
        stopControl,
        skipToPreviousControl,
        playControl,
      ];
    else if (hasNext)
      return [
        playControl,
        stopControl,
        skipToNextControl,
      ];
    else
      return [
        stopControl,
        playControl,
      ];
  }

  @override
  Future<void> onReplaceQueue(List<MediaItem> queue) async {
    _queue = queue;
    _queueIndex = 0;
    AudioServiceBackground.setMediaItem(mediaItem);
    await _audioPlayer.setUrl(mediaItem.id);
    /*onPlay();
    _setState(state: BasicPlaybackState.playing);*/
  }

  @override
  void onPlayFromMediaId(String mediaId) async {
    debugPrint(
        "---------------------------------------------------------------------------------------------------------------------------------------------MEEEEEEEESSSSSSSAGE");

    int index = _queue.indexWhere((e) => e.id == mediaId);
    var diff = index - _queueIndex;
    debugPrint(
        "---------------------------------------------------------------------------------------------------------------------------------------------MEEEEEEEESSSSSSSAGE: $diff");
    await _skip(diff);
    onPlay();
  }
}
