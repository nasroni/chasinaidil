import 'dart:async' show Future;
import 'dart:convert';

import 'package:chasinaidil/data/song.dart';
import 'package:chasinaidil/elements/song_box.dart';
import 'package:chasinaidil/services/songs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/services.dart';
import 'package:popup_menu/popup_menu.dart';
import 'package:chasinaidil/services/preferences.dart';

/*class SharedPreferencesHelper {
  static final String _likedSongs = "likedSongs";

  static Future<List<String>> getLikedSongs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> response = prefs.getStringList(_likedSongs);
    return response;
  }

  static Future<bool> setLikedSongs(List<String> value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList(_likedSongs, value);
  }
}*/

Future<String> _loadSongListJson() async {
  return await rootBundle.loadString('assets/data/songs.json');
}

Future<List<String>> _loadLikedSongs() async {
  // return await SharedPreferencesHelper.getLikedSongs();
  return await Preferences.likedSongs;
}

Future loadSongList() async {
  String jsonString = await _loadSongListJson();
  final jsonResponse = json.decode(jsonString);

  var list = jsonResponse as List;
  List<Song> songList = list.map((i) => Song.fromJson(i)).toList();

  return songList;
}

Future loadLikedSongs() async {
  List<String> likedSongs = await _loadLikedSongs();
  if (likedSongs == null) {
    likedSongs = [];
  }
  return likedSongs;
}

class SongList extends StatefulWidget {
  final String albumId;

  SongList({this.albumId});

  SongListState createState() => SongListState();
}

class SongListState extends State<SongList> {
  List<Song> _albumSongList;
  List<Song> _allowedList;
  List<Song> _filteredSongList;
  String _text;
  bool _loaded = false;
  bool _onlyPsalms = false;
  bool _fts = false;
  List<String> likedSongs = [];
  GlobalKey btnKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    loadLikedSongs().then((r) => setState(() {
          likedSongs = r;
        }));

    /*loadSongList().then((r) => setState(() {
          _songList = r;
          if (widget.albumId == "0") {
            _albumSongList = _songList;
          } else if (widget.albumId == "17") {
            _albumSongList = _songList.where((e) {
              if (likedSongs.contains(e.id.toString())) {
                return true;
              }
              return false;
            }).toList();
          } else {
            _albumSongList =
                _songList.where((f) => f.albumId == widget.albumId).toList();
          }
          _filteredSongList = _albumSongList;
          _allowedList = _albumSongList;
          _loaded = true;
        }));*/
    loadAll();
  }

  void loadAll() async {
    if (widget.albumId == "0")
      _albumSongList = await Songs.list();
    else if (widget.albumId == "17") {
      _albumSongList = await Songs.list();
      _albumSongList = _albumSongList
          .where((e) => likedSongs.contains(e.id.toString()))
          .toList();
    } else
      _albumSongList = await Songs.listAlbum(int.parse(widget.albumId));

    _filteredSongList = _albumSongList;
    _allowedList = _albumSongList;

    if (this.mounted)
      setState(() {
        _loaded = true;
      });
  }

  void showPopup() {
    PopupMenu menu = PopupMenu(
        items: [
          MenuItem(
              title: 'дар матн',
              image: Icon(
                Icons.view_headline,
                color: _fts ? Colors.amber[300] : Colors.white,
              )),
          MenuItem(
              title: 'танҳо забур',
              image: Icon(
                Icons.music_note,
                color: _onlyPsalms ? Colors.amber[300] : Colors.white,
              )),
        ],
        onClickMenu: onClickMenu,
        stateChanged: stateChanged,
        onDismiss: onDismiss,
        maxColumn: 1);
    menu.show(widgetKey: btnKey);
  }

  void stateChanged(bool isShow) {
    print('menu is ${isShow ? 'showing' : 'closed'}');
  }

  void onClickMenu(MenuItemProvider item) {
    if (item.menuTitle == 'танҳо забур') {
      setState(() {
        _onlyPsalms = !_onlyPsalms;
        if (_onlyPsalms) {
          _allowedList =
              _albumSongList.where((i) => isNumeric(i.fromText)).toList();
        } else {
          _allowedList = _albumSongList;
        }
      });
      reSearch();
    } else if (item.menuTitle == 'дар матн') {
      setState(() {
        _fts = !_fts;
      });
      reSearch();
    }
  }

  void onDismiss() {
    print('Menu is dismiss');
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return num.tryParse(s) != null;
  }

  void likeSong(int intId) {
    String id = intId.toString();
    setState(() {
      if (likedSongs.contains(id)) {
        likedSongs.remove(id);
      } else {
        likedSongs.add(id);
      }
    });
    Preferences.setLikedSongs(likedSongs);
  }

  void reSearch() {
    if (!_fts) {
      setState(() {
        if (_text == null) {
          _filteredSongList = _allowedList;
        } else {
          _filteredSongList = _allowedList.where((i) {
            bool titleTest = i.title.toLowerCase().contains(_text);
            bool idTest = i.id == int.tryParse(_text);
            if (_onlyPsalms) {
              bool psalmTest = i.fromText.contains(_text);
              return titleTest || idTest || psalmTest;
            }
            return titleTest || idTest;
          }).toList();
        }
      });
    } else {
      setState(() {
        if (_text == null) {
          _filteredSongList = _allowedList;
        } else {
          _filteredSongList = _allowedList.where((i) {
            var text = "";
            i.lyrics.forEach((f) => text += f.content);

            return text.toLowerCase().contains(_text.toLowerCase());
          }).toList();
        }
      });
    }
  }

  Widget slideFavoriteBackground() {
    return Container(
      color: Colors.amber[300],
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SizedBox(
              width: 20,
            ),
            Text(
              " Дӯст дорам",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Icon(
                Icons.star,
                color: Colors.white,
              ),
            ),
          ],
        ),
        alignment: Alignment.centerRight,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    PopupMenu.context = context;

    return Column(
      children: <Widget>[
        Container(
          height: 40,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 4),
                child: IconButton(
                  tooltip: 'Танзимоти ҷустуҷӯи',
                  icon: Icon(
                    Icons.settings,
                    key: btnKey,
                    //size: 25,
                  ),
                  onPressed: () => showPopup(),
                ),
              ),
              Expanded(
                child: TextField(
                  autocorrect: false,
                  decoration: InputDecoration(
                      suffixIcon: Icon(
                        Icons.search,
                      ),
                      contentPadding:
                          EdgeInsets.only(top: 15, bottom: 15, right: 46)),
                  onChanged: (text) {
                    text = text.toLowerCase();
                    setState(() {
                      _text = text;
                    });
                    reSearch();
                  },
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: _loaded
              ? _albumSongList != []
                  ? ListView.builder(
                      itemBuilder: (context, position) {
                        Song _song = _filteredSongList[position];
                        return Dismissible(
                          key: Key(_filteredSongList[position].id.toString()),
                          confirmDismiss: (direction) async {
                            int id = _filteredSongList[position].id;
                            likeSong(id);
                            return false;
                          },
                          background: slideFavoriteBackground(),
                          direction: DismissDirection.endToStart,
                          child: SongBox(
                            song: _song,
                            liked: likedSongs.contains(
                                _filteredSongList[position].id.toString()),
                          ),
                        );
                      },
                      itemCount: _filteredSongList.length,
                    )
                  : Container(
                      color: Colors.grey[50],
                      child: Center(
                        child: Text('Суруди дӯстдошта надорад ...'),
                      ),
                    )
              : Center(
                  child: CircularProgressIndicator(),
                ),
        ),
      ],
    );
  }
}
