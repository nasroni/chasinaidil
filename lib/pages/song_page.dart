import 'package:chasinaidil/data/song.dart';
import 'package:chasinaidil/elements/audio_player.dart';
import 'package:chasinaidil/elements/song_text_view.dart';
import 'package:chasinaidil/elements/swipe_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:swipedetector/swipedetector.dart';

Color colorBlue = Color.fromRGBO(0, 46, 171, 1.0);

class SongPage extends StatefulWidget {
  static const routeName = '/song';

  @override
  _SongPageState createState() => _SongPageState();
}

class _SongPageState extends State<SongPage> {
  bool fullScreen = false;
  double scaleFactor = 1.0;
  double oldScaleFactor;
  int whichView = 0; // 0 -> Text, 1 -> Accords, 2 -> Sheet
  List<IconData> viewIcon = [
    FontAwesomeIcons.alignCenter,
    FontAwesomeIcons.guitar,
    FontAwesomeIcons.music
  ];

  @override
  Widget build(BuildContext context) {
    double textSize = (16 * scaleFactor);
    bool darkMode =
        MediaQuery.of(context).platformBrightness == Brightness.dark;
    final Song args = ModalRoute.of(context).settings.arguments;

    SwipeAppBar songAppBar = fullScreen
        ? null
        : SwipeAppBar(
            onSwipeDown: () => Navigator.pop(context),
            appBar: AppBar(
              leading: IconButton(
                onPressed: () => Navigator.of(context).pop(),
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: Colors.amber[200],
                  size: 30,
                ),
              ),
              title: Text(
                args.id.toString() + " - " + args.title,
                style: TextStyle(fontSize: 20, fontFamily: 'Pacifico'),
              ),
              actions: <Widget>[
                kReleaseMode
                    ? Container()
                    : IconButton(
                        iconSize: 25,
                        onPressed: () => setState(() {
                          whichView += 1;
                          if (whichView == 3) {
                            whichView = 0;
                          }
                        }),
                        icon: Icon(
                          viewIcon[whichView],
                          size: 25,
                          color: Colors.amber[200],
                        ),
                      ),
              ],
            ),
          );

    return Scaffold(
      appBar: songAppBar,
      body: GestureDetector(
        onDoubleTap: () {
          setState(() {
            fullScreen = !fullScreen;
          });
          fullScreen
              ? SystemChrome.setEnabledSystemUIOverlays([])
              : SystemChrome.setEnabledSystemUIOverlays(
                  [SystemUiOverlay.bottom, SystemUiOverlay.top]);
        },
        onScaleStart: (scaleDetails) =>
            setState(() => oldScaleFactor = scaleFactor),
        onScaleUpdate: (ScaleUpdateDetails scaleDetails) {
          setState(() {
            double newScaleFactor = (oldScaleFactor * scaleDetails.scale);
            if (newScaleFactor <= 4 && newScaleFactor >= 0.4) {
              scaleFactor = newScaleFactor;
            }
          });
        },
        child: Container(
          child: Column(
            children: <Widget>[
              fullScreen
                  ? Container(
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(
                          args.id.toString() + " - " + args.title,
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 20,
                              color: colorBlue,
                              fontFamily: 'Pacifico'),
                        ),
                      ),
                    )
                  : Container(),
              fullScreen
                  ? Divider(
                      color: colorBlue,
                      height: 4,
                      thickness: 2,
                    )
                  : Container(),
              Expanded(
                child: SwipeDetector(
                  onSwipeRight: () => Navigator.pop(context),
                  child: whichView == 2
                      ? Container(
                          child: Center(
                              child: Text(
                                  'music sheet for piano, not yet implemented')),
                        )
                      : whichView == 1
                          ? SongTextView(
                              chords: true,
                              lyrics: args.lyrics,
                              textSize: textSize,
                              darkMode: darkMode)
                          : SongTextView(
                              chords: false,
                              lyrics: args.lyrics,
                              textSize: textSize,
                              darkMode: darkMode),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: fullScreen ? Container() : PlayActionButton.song(args.id, int.parse(args.albumId)),
      bottomNavigationBar: fullScreen ? BottomAppBar() : AudioBar(),
    );
  }
}
