import 'package:audio_service/audio_service.dart';
import 'package:chasinaidil/data/lists.dart';
import 'package:chasinaidil/elements/album_box.dart';
import 'package:chasinaidil/elements/audio_player.dart';
import 'package:chasinaidil/elements/song_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';

class StartPage extends StatelessWidget {
  final albumList = Lists.album;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          leading: Container(),
          title: Center(child: Text('Хазинаи дил')),
          actions: <Widget>[
            IconButton(
              onPressed: () => Navigator.pushNamed(context, '/settings'),
              tooltip: 'Танзимот',
              icon: Icon(
                Icons.info_outline,
                color: Colors.amber[200],
              ),
            ),
          ],
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.home),
              ),
              Tab(
                icon: Icon(Icons.format_list_bulleted),
                //text: 'Ҳама',
              ),
              Tab(
                icon: Icon(Icons.star),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ResponsiveGridList(
                desiredItemWidth: 150,
                minSpacing: 10,
                children: albumList
                    .map((album) => AlbumBox(
                          id: album["id"],
                          title: album["title"],
                          cover: album["cover"],
                          action: "open",
                        ))
                    .toList(),
              ),
            ),
            Container(
              child: SongList(
                albumId: "0",
              ),
            ),
            Container(
              child: SongList(
                albumId: "17",
              ),
            ),
          ],
        ), // This trailing comma makes auto-formatting nicer for build methods.
        bottomNavigationBar: AudioBar(),
      ),
    );
  }
}
