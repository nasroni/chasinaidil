import 'package:chasinaidil/data/lists.dart';
import 'package:chasinaidil/elements/album_box.dart';
import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';

class DownloadPage extends StatefulWidget {
  static const routeName = '/download';

  _DownloadPageState createState() => _DownloadPageState();
}

class _DownloadPageState extends State<DownloadPage> {
  final albumList = Lists.album;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return _goToHome(context);
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () => _goToHome(context),
            icon: Icon(
              Icons.arrow_back,
              color: Colors.amber[200],
            ),
          ),
          centerTitle: true,
          title: Text(
            'Боргирӣ',
            style: TextStyle(fontSize: 25, fontFamily: 'Pacifico'),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ResponsiveGridList(
            desiredItemWidth: 150,
            minSpacing: 10,
            children: albumList
                .map((album) => AlbumBox(
                      id: album["id"],
                      title: album["title"],
                      cover: album["cover"],
                      action: "download",
                    ))
                .toList(),
          ),
        ),
      ),
    );
  }

  bool _goToHome(BuildContext context) {
    Navigator.popUntil(context, (route) => route.isFirst);
    return true;
  }
}
