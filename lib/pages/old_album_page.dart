import 'package:chasinaidil/data/album_info.dart';
import 'package:chasinaidil/elements/song_list.dart';
import 'package:chasinaidil/elements/swipe_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:swipedetector/swipedetector.dart';

class OldAlbumPage extends StatefulWidget {
  static const routeName = '/album';

  _OldAlbumPageState createState() => _OldAlbumPageState();
}

class _OldAlbumPageState extends State<OldAlbumPage> {
  OverlayEntry imageViewer;
  bool isImageViewerVisible = false;

  OverlayEntry _createImageViewer(String logo) {
    MediaQueryData media = MediaQuery.of(context);

    return OverlayEntry(
      builder: (context) => Positioned(
        left: 0,
        top: 0,
        child: GestureDetector(
          onTap: () {
            this.imageViewer.remove();
            isImageViewerVisible = false;
          },
          child: Material(
              type: MaterialType.transparency,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                ),
                height: media.size.height,
                width: media.size.width,
                child: Center(
                  child: Container(
                      height: media.orientation == Orientation.portrait
                          ? media.size.width
                          : media.size.height,
                      width: media.orientation == Orientation.portrait
                          ? media.size.width
                          : media.size.height,
                      child: Image.asset('assets/covers/cd_' + logo)),
                ),
              )),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(
        [SystemUiOverlay.top, SystemUiOverlay.bottom]);
    final AlbumInfo args = ModalRoute.of(context).settings.arguments;

    return GestureDetector(
      onTap: () {
        this.imageViewer.remove();
        isImageViewerVisible = false;
      },
      child: Scaffold(
        appBar: SwipeAppBar(
          onSwipeRight: () => Navigator.of(context).pop(),
          appBar: AppBar(
            leading: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.arrow_back,
                color: Colors.amber[200],
              ),
            ),
            centerTitle: true,
            title: Text(
              args.title,
              style: TextStyle(fontSize: 25, fontFamily: 'Pacifico'),
            ),
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  if (isImageViewerVisible) {
                    this.imageViewer.remove();
                    isImageViewerVisible = false;
                  }
                  this.imageViewer = _createImageViewer(args.cover);
                  Overlay.of(context).insert(this.imageViewer);
                  isImageViewerVisible = true;
                },
                child: Image.asset('assets/covers/cd_' + args.cover),
              ),
            ],
          ),
        ),
        body: SwipeDetector(
          onSwipeRight: () => Navigator.pop(context),
          child: Container(
            child: SongList(albumId: args.id.toString()),
          ),
        ),
      ),
    );
  }
}
