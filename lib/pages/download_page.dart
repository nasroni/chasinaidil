import 'dart:io';

import 'package:chasinaidil/data/lists.dart';
import 'package:chasinaidil/elements/download_item.dart';
import 'package:chasinaidil/services/audiofiles.dart';
import 'package:chasinaidil/services/preferences.dart';
import 'package:chasinaidil/services/songs.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class DownloadPage extends StatefulWidget {
  static const routeName = '/download';

  DownloadPageState createState() => DownloadPageState();
}

class DownloadPageState extends State<DownloadPage> {
  bool _allowWriteFile = false;

  final albumList = Lists.albumWF;
  var downloadState = {
    0: {"state": false, "percentage": 0},
    1: {"state": false, "percentage": 0},
    2: {"state": false, "percentage": 0},
    3: {"state": false, "percentage": 0},
    4: {"state": false, "percentage": 0},
    5: {"state": false, "percentage": 0},
    6: {"state": false, "percentage": 0},
    7: {"state": false, "percentage": 0},
    8: {"state": false, "percentage": 0},
    9: {"state": false, "percentage": 0},
    10: {"state": false, "percentage": 0},
    11: {"state": false, "percentage": 0},
    12: {"state": false, "percentage": 0},
    13: {"state": false, "percentage": 0},
    14: {"state": false, "percentage": 0},
    15: {"state": false, "percentage": 0},
    16: {"state": false, "percentage": 0},
  };

  @override
  void initState() {
    requestWritePermission();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return _goToHome(context);
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () => _goToHome(context),
            icon: Icon(
              Icons.arrow_back,
              color: Colors.amber[200],
            ),
          ),
          centerTitle: true,
          title: Text(
            'Боргирӣ',
            style: TextStyle(fontSize: 25, fontFamily: 'Pacifico'),
          ),
        ),
        body: Container(
          child: ListView.builder(
            itemCount: albumList.length,
            itemBuilder: (BuildContext context, int i) {
              var a = albumList[i];
              return DownloadItem(
                i,
                albumName: a["title"],
                image: a["cover"],
                downloadPercentage: downloadState[i]["percentage"],
                downloading: downloadState[i]["state"],
                clickAction: (int id) => downloadAlbum(id),
              );
            },
          ),
        ),
      ),
    );
  }

  void downloadAlbum(int id) {
    startDownloadAlbum(id);
    setState(() {
      downloadState[id]["state"] = true;
    });
    /*setState(() {
      downloadState[id]["percentage"] =
          (int.parse(downloadState[id]["percentage"].toString()) + 1);
    });*/
  }

  bool _goToHome(BuildContext context) {
    bool downloading = false;
    downloadState.forEach((key, value) {
      if (value["state"]) downloading = true;
    });
    if (downloading == false) {
      Navigator.popUntil(context, (route) => route.isFirst);
      dispose();
      return true;
    } else {
      Toast.show('Илтимос интизор шавед ...', context);
      return false;
    }
  }

  Future _albumFolder(String id, bool create) async {
    var folder = await Audiofiles.albumFolder(id, create);
    return folder;
  }

  downloadFile(int albumId, String id, HttpClient client) async {
    if (!_allowWriteFile) return false;
    while (id.length != 3) id = "0" + id;

    var folder = await _albumFolder(albumId.toString(), true);

    var remoteFileString =
        'https://${Audiofiles.SONG_SERVER}/${Audiofiles.SONG_PREFIX}$id.mp3';
    var fileString = '$folder/${Audiofiles.SONG_PREFIX}$id.mp3';

    await client
        .getUrl(Uri.parse(remoteFileString))
        .then((HttpClientRequest request) => request.close())
        .then((HttpClientResponse response) =>
            response.pipe(new File(fileString).openWrite()));
  }

  Future<bool> checkDownloadFile(int albumId, String id) async {
    var exists = await Audiofiles.checkDownloadFile(albumId, id);
    return exists;
  }

  startDownloadAlbum(int id) async {
    var songs;
    if (id == 0)
      songs = await Preferences.likedSongs;
    else
      songs = await Songs.listAlbumIds(id);
    var length = songs.length;
    var current =
        (int.parse(downloadState[id]["percentage"].toString()) * length / 100)
            .round();

    var client = HttpClient();
    for (var song in songs) {
      var exists = await checkDownloadFile(id, song);
      print(exists);
      if (!exists) {
        await downloadFile(id, song, client);
        current += 1;
        var percentage = (current * 100 / length).round();
        setState(() {
          downloadState[id]["percentage"] = percentage;
        });
      }
    }
    client.close();
    setState(() {
      downloadState[id]["state"] = false;
    });
  }

  checkDownloadAlbum(int id) async {
    var percentage = await Audiofiles.checkDownloadAlbum(id);
    setState(() {
      downloadState[id]["percentage"] = percentage;
    });
  }

  checkDownloadAll() async {
    for (var album in downloadState.keys) {
      checkDownloadAlbum(album);
    }
  }

  requestWritePermission() async {
    var success = await Audiofiles.requestWritePermission();
    if (success) {
      setState(() {
        _allowWriteFile = true;
        Audiofiles.allowWriteFile = true;
      });
      checkDownloadAll();
    }
  }
}
