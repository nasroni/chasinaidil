import 'dart:ui';

import 'package:chasinaidil/data/album_info.dart';
import 'package:chasinaidil/elements/audio_player.dart';
import 'package:chasinaidil/elements/song_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AlbumPage extends StatefulWidget {
  static const routeName = '/album';

  _AlbumPageState createState() => _AlbumPageState();
}

class _AlbumPageState extends State<AlbumPage> {
  int played = 0;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(
        [SystemUiOverlay.top, SystemUiOverlay.bottom]);
    final AlbumInfo args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: Material(
        child: GestureDetector(
          onHorizontalDragEnd: (e) {
            if (e.primaryVelocity >= 1500) Navigator.of(context).pop();
          },
          child: CustomScrollView(
            anchor: 0,
            slivers: <Widget>[
              SliverAppBar(
                snap: true,
                floating: true,
                pinned: true,
                leading: IconButton(
                  onPressed: () => Navigator.of(context).pop(),
                  icon: Icon(
                    Icons.arrow_back,
                    size: 35,
                    color: Colors.amber[400],
                  ),
                ),

                //title: Text('testtext'),
                centerTitle: true,
                expandedHeight: 250,
                stretch: true,
                flexibleSpace: FlexibleSpaceBar(
                  stretchModes: <StretchMode>[
                    StretchMode.fadeTitle,
                    StretchMode.blurBackground
                  ],
                  collapseMode: CollapseMode.parallax,
                  titlePadding: EdgeInsets.all(10),
                  centerTitle: true,
                  title: Text(
                    args.title,
                    maxLines: 1,
                    style: TextStyle(
                      fontSize: 25,
                      fontFamily: 'Pacifico',
                      background: new Paint()
                        ..maskFilter = MaskFilter.blur(BlurStyle.normal, 30)
                        ..color = Colors.black54,
                    ),
                  ),
                  background: Image.asset(
                    'assets/covers/cd_hq_' + args.cover,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SliverFillRemaining(
                //child: SafeArea(
                child: SongList(
                  albumId: args.id.toString(),
                ),
                //),
              )
            ],
          ),
        ),
      ),
      //floatingActionButton: AudioFloatingActionButton(albumId: args.id),
      bottomNavigationBar: AudioBar(
        albumId: args.id,
      ),
      floatingActionButton: PlayActionButton(args.id),
    );
  }
}
