import 'package:chasinaidil/elements/swipe_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool darkMode =
        MediaQuery.of(context).platformBrightness == Brightness.dark;

    return Scaffold(
      appBar: SwipeAppBar(
        onSwipeRight: () => Navigator.of(context).pop(),
        appBar: AppBar(
          leading: IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(
              Icons.arrow_back,
              color: Colors.amber[200],
            ),
          ),
          centerTitle: true,
          title: Text(
            'Дар бораи мо',
            style: TextStyle(fontSize: 25, fontFamily: 'Pacifico'),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.file_download,
                color: Colors.amber[200],
              ),
              onPressed: () => Navigator.pushNamed(context, '/download'),
            ),
          ],
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: RichText(
                      text: TextSpan(
                        style: TextStyle(
                          color: darkMode ? Colors.white : Colors.black,
                        ),
                        children: [
                          TextSpan(
                            text: 'Пешгуфтори нашри якуми соли 2000\n',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Имондорон ҳамеша, дар ҳама ҷаҳон, барои Худо аз ҷону дил сурудҳо мехонанд. Миннатдори аз корҳои Худо ва муҳаббат нисбати шахсияти Ӯ боиси таронасароии бандагонаш аст: дар танҳои, дар ҷамоат, дар оила ва ҳангоми кор.\n\nИмондорон ҳамеша, дар ҳама ҷаҳон, барои Худо аз ҷону дил сурудҳо мехонанд. Миннатдори аз корҳои Худо ва муҳаббат нисбати шахсияти Ӯ боиси таронасароии бандагонаш аст: дар танҳои, дар ҷамоат, дар оила ва ҳангоми кор.\n\nМо умедворем, ки ин суруднома барои имондорон ёрие бишавад, то ки онҳоро ба сароидани сурудҳои нав барои ҷалоли Худо водор намояд.\n\nБаракати Худо бар ҳамаи шумо бод!\n\n',
                          ),
                          TextSpan(
                            text: 'Пешгуфтори нашри дуюми соли 2004\n',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                              text:
                                  'Нашри дуввуми сурудномаи масеҳии “Хазинаи дил” бо ёрии Парвардигор тайёр карда шудааст. Ҳаҷми китоб хурдтар, вале сифаташ баландтар шудааст. Як қисми сурудҳо партофта шудааст, қисми дигар ислоҳ карда шудааст. Якчанд сурудҳо илова карда шудаанд, ки аксаран аз таронаҳои Забур мебошанд. Илова ба ин китоб ҳамчунин китоби дигаре нашр карда шудааст, ки ҳам матн ва ҳам нотаҳои сурудҳоро дорад. Сурудҳои беоҳанг бо ситорача (*) ишора карда шудаанд.\n\nЧуноне ки аз таърих маълум аст, дар давоми 1000 сол масеҳият дар  Осиёи Миёна ривоҷу равнақе доштааст. Вале бо сабабҳои гуногуни таърихи дар охири асрҳои миёна аз байн рафтааст. Баъд аз истиқлолияти Тоҷикистон масеҳияти тоҷик умри дубора пайдо кард. Мо умед дорем, ки ин суруднома барои устувор кардани масеҳияти тоҷик ёри мерасонад.\n\nМуҳаррирони сурудномаи “Хазинаи дил”'),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Divider(),
            Container(
              height: 150,
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    style: TextStyle(color: Colors.black),
                    children: [
                      TextSpan(
                          text: 'Хазинаи дил\n',
                          style: TextStyle(fontSize: 30)),
                      TextSpan(text: 'аз\n', style: TextStyle(fontSize: 18)),
                      TextSpan(
                        text: 'www.isoimaseh.com',
                        style: TextStyle(fontSize: 23, color: Colors.blue),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () async {
                            await launch('https://isoimaseh.com');
                          },
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
