import 'dart:io';

import 'package:audio_service/audio_service.dart';
import 'package:chasinaidil/data/lists.dart';
import 'package:chasinaidil/data/song.dart';
import 'package:chasinaidil/services/preferences.dart';
import 'package:chasinaidil/services/songs.dart';
import 'package:flutter/cupertino.dart';
import 'package:permission_handler/permission_handler.dart';

class Audiofiles {
  static const String SONG_PREFIX = 'Хазинаи-дил-';
  static const String FOLDER_PREFIX = 'Хазинаи дил ';
  static const String FOLDER_FAVORITES = 'Дӯстдошта';
  static const String SONG_SERVER = 'chasinaidil.nasroni.one/mp3/all';
  static const String EXT_DIR = '/storage/emulated/0';
  static const String FOLDER = 'Хазинаи дил';

  static bool allowWriteFile = false;

  static Future get _localFolder async {
    var localFolder = Directory('$EXT_DIR/$FOLDER');
    if (await localFolder.exists()) return localFolder.path;
    final Directory newLocalFolder = await localFolder.create(recursive: false);
    return newLocalFolder.path;
  }

  static Future albumFolder(String id, bool create) async {
    if (!allowWriteFile) return false;
    var localPath = await _localFolder;
    while (id.length != 2) id = "0" + id;
    var tempFolder = Directory('$localPath/$FOLDER_PREFIX$id');
    if (id == "00") tempFolder = Directory('$localPath/$FOLDER_FAVORITES');
    if (await tempFolder.exists()) return tempFolder.path;
    if (create) {
      final Directory newAlbumFolder = await tempFolder.create(recursive: true);
      return newAlbumFolder.path;
    }
    return null;
  }

  static Future<bool> checkDownloadFile(int albumId, String id) async {
    if (!allowWriteFile) return false;
    while (id.length != 3) id = "0" + id;

    var folder = await albumFolder(albumId.toString(), false);
    var fileString = '$folder/$SONG_PREFIX$id.mp3';
    final file = File(fileString);
    final bool exists = file.existsSync();
    return exists;
  }

  static Future<int> checkDownloadAlbum(int id) async {
    List<String> songs;
    if (id == 0) {
      songs = await Preferences.likedSongs;

      if (songs.length == 0) {
        return 100;
      }
    } else {
      songs = await Songs.listAlbumIds(id);
    }
    var length = songs.length;
    var count = 0;
    for (var song in songs) {
      while (song.length != 3) song = "0" + song;
      bool fileExists = await checkDownloadFile(id, song);
      if (fileExists) {
        count++;
      }
    }
    int percentage = (count * 100 / length).round();
    return percentage;
  }

  static Future<MediaItem> getMediaItem(Song song, int albumIdI) async {
    var id = song.id.toString();
    while (id.length != 3) id = "0" + id;

    var folder = await albumFolder(song.albumId.toString(), false);
    var fileString = 'file://$folder/$SONG_PREFIX$id.mp3';

    var albumId = song.albumId;
    var albumString = "CD $albumId";
    while (albumId.length != 2) albumId = "0" + albumId;
    var artString = "https://chasinaidil.nasroni.one/covers/cd_hq_$albumId.jpg";

    var mediaitem = MediaItem(
        album: albumString,
        id: fileString,
        title: song.id.toString() + " " + song.title,
        artist: "www.IsoiMaseh.com",
        artUri: artString,
        duration: song.durationInt,
        genre: albumIdI.toString()
    );
    return mediaitem;
  }

  static Future<List<MediaItem>> getSong(int songI, int albumIdI) async {
    List<Song> songs = await Songs.listAlbum(albumIdI);
    Song song = songs.where((e) => e.id == songI).toList()[0];
    return [await getMediaItem(song, albumIdI)];
  }

  static Future<List<MediaItem>> getQueue(int albumIdI) async {
    List<MediaItem> mediaItems = List<MediaItem>();
    /*if(id == 0) {
      ids = await Preferences.likedSongs;
      if(ids.length == 0) return List<MediaItem>();
    }*/
    List<Song> songs = await Songs.listAlbum(albumIdI);

    for (var song in songs){
      mediaItems.add(await getMediaItem(song, albumIdI));
    }
    return mediaItems;
  }

  static Future<bool> requestWritePermission() async {
    var handler = PermissionHandler();
    var permission =
        await handler.checkPermissionStatus(PermissionGroup.storage);
    if (permission != PermissionStatus.granted) {
      // Requesting Rights
      var permissions =
          await handler.requestPermissions([PermissionGroup.storage]);
      if (permissions[PermissionGroup.storage] != PermissionStatus.granted) {
        // Rights denied
        return false;
      } else {
        // Rights granted
        allowWriteFile = true;
        return true;
      }
    } else {
      // Rights already granted
      allowWriteFile = true;
      return true;
    }
  }
}
