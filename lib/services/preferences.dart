import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static const String _LIKED_SONGS = "likedSongs";

  static Future<List<String>> get likedSongs async {
    var res = await SharedPreferences.getInstance()
        .then((v) => v.getStringList(_LIKED_SONGS));
    if (res == null) res = [];
    return res;
  }

  static Future<bool> setLikedSongs(List<String> value) async {
    var res = await SharedPreferences.getInstance()
        .then((v) => v.setStringList(_LIKED_SONGS, value));
    return res;
  }
}
