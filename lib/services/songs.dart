import 'dart:async';
import 'package:flutter/services.dart' show rootBundle;
import 'package:chasinaidil/data/song.dart';
import 'dart:convert';

class Songs {
  static Future<String> _loadSongListJson() async {
    if (_jsonCache == null)
      _jsonCache = await rootBundle.loadString('assets/data/songs.json');
    return _jsonCache;
  }

  static String _jsonCache;

  static Future<List<Song>> list() async {
    String jsonString = await _loadSongListJson();
    final jsonResponse = json.decode(jsonString);

    var list = jsonResponse as List;
    List<Song> songList = list.map((i) => Song.fromJson(i)).toList();

    return songList;
  }
  
  static Future<Song> song(int id) async {
    var _songs = await list();
    return _songs.singleWhere((element) => element.id == id);
  }

  static Future<List<Song>> listAlbum(int albumId) async {
    var _songs = await list();
    return _songs.where((Song e) => e.albumId == albumId.toString()).toList();
  }

  static Future<List<String>> listAlbumIds(int albumId) async {
    List<Song> _songs;
    var _songIdList = new List<String>();
    if (albumId == 0)
      _songs = await list();
    else
      _songs = await listAlbum(albumId);
    _songs.forEach((Song song) {
      _songIdList.add(song.id.toString());
    });
    // ((Song song) => song.id.toString());
    return _songIdList;
  }

  static Future<List<String>> listIds() async {
    return await listAlbumIds(0);
  }

  Songs();
}
